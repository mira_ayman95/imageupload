var CronJob = require('cron').CronJob;
var session = require('./jobs/sessions.js');
var superadmin =require('./jobs/superadmin.js');

module.exports = function(){
    session(CronJob);
    superadmin(CronJob);
}