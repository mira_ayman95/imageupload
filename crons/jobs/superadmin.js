var Roles = require('../../static_arch/roles');
var User = require('../../models/user.js');
var Logger = require('../../modules/logger.js');
var Config = require('../../config');

module.exports = function(CronJob){

    //comment if needed [start -a1 ] if you don't want superadmin to be removed automaticly
    var job = new CronJob({
        //runs every saturaday at 3 am;
        cronTime: '00 00 03 * * 6',
        onTick: function() {
            Logger.trace('highlight', 'cronjob', `remove all superadmins`);
            User.find({role:Roles.raw.master}).remove().exec();
        },
        start: false,
        timeZone: Config.timeZone
    });
    job.start();
    //comment if needed [start -a1 ]

}