var Config = require('../../config');
var Session = require('../../models/session.js');
var timeFactory = require('../../modules/time-factory.js');
var Logger = require('../../modules/logger.js');

module.exports = function(CronJob){

    
    var job = new CronJob({
        //runs every day at 3:00:00 AM
        cronTime: '00 00 03 * * 0-6',
        onTick: function() {
            Logger.trace('highlight', 'SESSION/ CRONJOB', `cronjob 🕒 ${new Date()}`);
            Logger.trace('info', 'session/cronjob', `removeling less than ${timeFactory.cal('remove', 5, 'minutes', new Date())}`);

            //keep active session during the last 6 hours
            var d = timeFactory.cal('remove', 6, 'hours', new Date()).toISOString();
            Session.find({updatedAt:{ $lt: d} }).remove().exec();
        },
        start: false,
        timeZone: Config.timeZone
    });
    job.start();

}