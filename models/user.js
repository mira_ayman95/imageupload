var mongoose = require('mongoose');
var bcrypt   = require('bcryptjs');
var jwt      = require('jsonwebtoken');
var config   = require('../config');
var Roles     = require('../static_arch/roles.js');

var User = mongoose.Schema({
    username  : String,
    password  : String,
	role      : { type: String, default: Roles.raw.basic},
	createdAt : { type: Date, default: Date.now }
});

//generatePasswordHASH
User.methods.generateHash = function(password){
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//checkPasswordValidation
User.methods.isValidPassword = function(password){
	return bcrypt.compareSync(password, this.password);
};

//generateSignedTicketForUser
User.methods.getTicketData = function(){
    //add addational payload 
	var data = {};
    data._id = this._id;
    data.role = this.role;
	return data;
}

User.methods.add = function(obj){
	this.username = obj.username;
	this.password = this.generateHash(obj.password);
}

User.methods.addSuper = function(obj){
	this.role = Roles.raw.master;
	this.username = obj.username;
	this.password = this.generateHash(obj.password);
}


module.exports = mongoose.model('User', User);