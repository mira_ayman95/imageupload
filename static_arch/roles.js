//master is created with these defaults 
var masterDefaults = { 
	username: process.env.SUPER_USERNAME, 
	password: process.env.SUPER_PASSWORD
}; 

//who can register for whome
var regArch = {
	//superadmin can register for admin
	superadmin: ['admin'],
	//admin can register for supervisor and user
	admin: ['supervisor', 'user'],
	//supervisor can register for user
	supervisor: ['user']
}

var roles = {
	//A master will be created when the app starts
	//using default master credentials  
	master: 'superadmin',

	// basic user is used as a fallback for un assigned roles
	basic: 'user',

	//other roles 
	others: ['admin','supervisor']
};

//return all roles
function rolesList(roles){
	var rolesList = roles.others;
	rolesList.push(roles.master,roles.basic);
	return rolesList; 
}

module.exports = {
	list: rolesList(roles),
	raw: roles,
	defaults: masterDefaults,
	regArch: regArch
}