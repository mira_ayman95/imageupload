
var length =  {
		username: {
			min: 8,
			max: 12
		},
		title: {
			min: 3,
			max: 30
		},
		flagTitle: {
			min: 4,
			max: 35
		},
		name: {
			min:3,
			max:20
		},
		desc: {
			min:3,
			max:300
		},
		fullname: {
			min: 10,
			max: 30
		},
		password: {
			min: 6,
			max: 10
		},
		pin: {
			min: 7,
			max: 15
		},
		job: {
			min: 4,
			max: 30
		},
		address: {
			min: 10,
			max: 70
		},
		mobile:{
			min:8,
			max:16
		},
		productName:{
			min: 6,
			max: 80
		},
		details: {
			min: 10, 
			max: 300
		},
		day: {
			min: 1,
			max: 2
		},
		month: {
			min: 1,
			max: 2
		},
		year: {
			min:4, 
			max:4
		}
}

var regex = {

	url: "((https?:\\/\\/(?:www\\.|(?!www)))?[^\\s\\.]+\\.[^\\s]{2,}|www\\.[^\\s]+\\.[^\\s]{2,})",
	email: "^[-a-z0-9~!$%^&*_=+}{\\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\\'?]+)*@([a-z0-9_][-a-z0-9_]*(\\.[-a-z0-9_]+)*\\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?$",
	username: "^[a-zA-Z0-9_-]{" + length.username.min + "," + length.username.max + "}$",
	pin: "^[0-9]{" + length.pin.min + "," + length.pin.max + "}$",
	password: "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{" + length.password.min + "," + length.password.max + "}$",
	hex: "^#?([a-f0-9]{6}|[a-f0-9]{3})$",
	slug: "^[a-z0-9-]+$",
	htmlTag: "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$",
	title: "^[a-zA-Z0-9 _-]{" + length.title.min + "," + length.title.max + "}$",
	flagTitle: "^[a-zA-Z0-9_-]{" + length.flagTitle.min + "," + length.flagTitle.max + "}$",
	name: "^[a-zA-Z]{" + length.name.min + "," + length.name.max + "}$",
	fullname: "^[a-zA-Z ]{" + length.fullname.min + "," + length.fullname.max + "}$",
	desc: "^((?![;<>{}]|((https?:\\/\\/(?:www\\.|(?!www)))?[^\\s\\.]+\\.[^\\s]{2,}|www\\.[^\\s]+\\.[^\\s]{2,})).){"+length.desc.min+","+length.desc.max+"}$",
	text: "^[a-zA-Z]$",
	number: "^[0-9]+$",
	ar_fullname: "^[\\u0621-\\u064A\\s]{8,80}$",
	eg_national_id: "^(2|3)[0-9][1-9][0-1][1-9][0-3][1-9](01|02|03|04|11|12|13|14|15|16|17|18|19|21|22|23|24|25|26|27|28|29|31|32|33|34|35|88)\\d\\d\\d\\d\\d$",
	eg_mobile:"^01[0-2]{1}[0-9]{8}$",
	eg_landLine: "^(0)?[2]+[0-9][-]*(\\d{4})[-]*(\\d{3})$",
	coordinates : "^-?[0-9]{1,3}[.][0-9]{1,}$",
	nd: function(nf,nt,df,dt){return `^[0-9]{${nf},${nt}}(\\.[0-9]{${df},${dt}})?$`},
	mongoId: "^(?=[a-f\\d]{24}$)(\\d+[a-f]|[a-f]+\\d)"
}
	
module.exports = {
	length: length,
	regex: regex
}
