
global.supertest          = require("supertest");
global.config             = require('../../config');
global.server             = supertest.agent(config.host);
global.helper             = require('../helper.js');

describe("System Admin API",function(){

  this.timeout(10000); 
  var ticket = ''; 

  var users = {
    user1: {
      username: "bahihussein",
      password: "@Bahi2012"
    }
  }
  
  it("should return access ticket",function(done){
        server
        .post('/api/access')
        .send({
          username: users.user1.username,
          password: users.user1.password
        })
        .expect("Content-type",/json/)
        .expect(200)
        .end(function(err,res){
            console.log(`sent:  ${JSON.stringify(users.user1)}`)
            if(res.body) console.log(res.body);
            ticket = res.body.ticket;
            res.status.should.equal(200);
            done();
        });
  });

});