
var supertest          = require("supertest");
var config             = require('../../config');
var server             = supertest.agent(config.host);
var helper             = require('../helper.js');

describe("User API",function(){

  this.timeout(10000); 
  var ticket = ''; 

  var users = {
    user1: {
      username: helper.generate('all-alpha', 8),
      password: helper.generate('mix', 8)
    },
    user2: {
      username: helper.generate('all-alpha', 8),
      password: helper.generate('mix', 8)
    }
  }

  it("should create user",function(done){
        server
        .post('/api/user')
        .send({
          username: users.user1.username,
          password: users.user1.password
        })
        .expect("Content-type",/json/)
        .expect(200)
        .end(function(err,res){
            console.log(`sent:  ${JSON.stringify(users.user1)}`)
            if(res.body) console.log(res.body);
            res.status.should.equal(200);
            done();
        });
  });

  it("should return already exists error",function(done){
        server
        .post('/api/user')
        .send({
          username: users.user1.username,
          password: users.user1.password
        })
        .expect("Content-type",/json/)
        .expect(400)
        .end(function(err,res){
            console.log(`sent:  ${JSON.stringify(users.user1)}`)
            if(res.body) console.log(res.body);
            res.status.should.equal(400);
            done();
        });
  });

  it("should return access ticket",function(done){
        server
        .post('/api/access')
        .send({
          username: users.user1.username,
          password: users.user1.password
        })
        .expect("Content-type",/json/)
        .expect(200)
        .end(function(err,res){
            console.log(`sent:  ${JSON.stringify(users.user1)}`)
            if(res.body) console.log(res.body);
            ticket = res.body.ticket;
            res.status.should.equal(200);
            done();
        });
  });

for(var i = 0; i < 2; i++){

  it("should return profile", function(done){
      server
      .get('/api/user')
      .set('ticket', ticket)
      .expect("Content-type",/json/)
      .expect(200)
      .end(function(err,res){
          if(res.body) console.log(res.body);
          res.status.should.equal(200);
          done();
      });
  });

}
  

});