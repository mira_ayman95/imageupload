var uCheck       = require('ucheck');
var Log          = require('../models/log');
var Security     = require('../security');
var Say          = require('../langs');

module.exports = {

    getLogs: function(opts={}){
        return function(req, res, next){
            Log.find({}).exec(function(err, logs){
                if(logs){
                    return res.json(logs);
                } else {
                    return res.status(400).json({error:Say.error.notFound});
                }
            })
        }
    }

}

