var multer = require('multer');
var AWS = require('aws-sdk');

var uCheck = require('ucheck');
var Validation = require('../static_arch/validation.js');
var Security = require('../security');
var cloudinary = require('cloudinary');
const path = require('path');

module.exports = {

    uploadImage: function (opts = {}) {
        return function (req, res, next) {
            var imageFile = req.file.filename;
            // console.log(imageFile);

            cloudinary.config({
                cloud_name: 'arab-sec',
                api_key: '588325171973447',
                api_secret: 'hrWT4fsCnszZ5KpFlgaF1SmkwsU'
            });

            function uploadImg(cb) {
                var file = { path: path.join(process.cwd(), './uploads/') + imageFile, name: "imageupload" }
                // console.log(file)
                cb(file.path, file.name)
            }
            uploadImg(function (path, name) {
                cloudinary.v2.uploader.upload(path, {
                    public_id: name,
                    // categorization: "google_tagging",
                    use_filename: true,
                    // unique_filename : false,

                    //for making croping photo 
                    transformation: [
                        {  effect: "pixelate_faces", width: 600, height: 600, crop: "crop", gravity: "face" },
                        { overlay: "text:roboto_25_bold:msh Angelina", gravity: "south", y: 20 },
                        { border: "3px_solid_black" }
                        // { crop: "crop", x: 5, y: 10, width: 40, height: 10 }
                    ],//for making media library photo cropped 
                    eager: {
                        width: 100, height: 100, crop: "crop", gravity: "face"
                    },
                    tags: ['special', 'for_homepage'],

                }, function (error, result) {
                    if (result) {
                        console.log(result)
                        return res.status(200).json({ message: "Your Profile Photo has been Uploaded Link: " + result.url });
                    } else {
                        return res.status(400).json({ error: "Profile Photo Not Uploaded, " + error.message });
                    }
                });
            });
        }
    }
}