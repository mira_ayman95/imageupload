var uCheck       = require('ucheck');
var Validation   = require('../static_arch/validation');
var User         = require('../models/user');
var Say          = require('../langs');
var Security     = require('../security');
var Logger       = require('../modules/logger');

module.exports = {
    login: function(opts={}){
        return function(req, res, next){

            Logger.trace('highlight', 'router/access post validation', 'before access...');
            var x = [{
                    param: 'username',
                    label: 'Username',
                    required: true,
                    type: 'string',
                    length: { min: Validation.length.username.min , max: Validation.length.username.max},
                    regex: Validation.regex.username
                },{
                    param: 'password',
                    label: 'Password',
                    required: true,
                    type: 'string',
                    length: { min: Validation.length.password.min , max: Validation.length.password.max},
                    regex: Validation.regex.password
                }]

            let ucheck = new uCheck.validate(req).scenario(x);


            if(ucheck.hasErrors()) {
                return res.status(400).json({error: ucheck.getErrors()});
            } else {

                Logger.trace('info', 'router/access post validation', 'requesting access...');
                var username = req.body.username;
                var password = req.body.password;
                
                User.findOne({ 'username' :  username }).exec(function(err, user) {
                
                    if (user) {
                        if (user.isValidPassword(password)){

                            Security.buildTicket(user, user.getTicketData(), function(ticket){
                                return res.json({ticket: ticket});
                            });

                        } else {
                            return res.status(400).json({error: "wrong credintaisl"});
                        }
                    } else {
                        return res.status(400).json({error: Say.error.notFound});
                    }

                });    
            }
        }
    }
}