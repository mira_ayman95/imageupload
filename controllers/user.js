

var uCheck       = require('ucheck');
var Validation   = require('../static_arch/validation.js');
var User         = require('../models/user');
var Say          = require('../langs');
var Security     = require('../security');

module.exports = {

    getProfile: function(opts={}){
        return function(req, res, next){
            User.findOne({_id: req.ticket.data._id}, 'username').exec(function(err, user){
                if(user){
                    return res.json(user);
                } else {
                    return res.status(400).json({error:Say.error.notFound});
                }
            })
        }
    },
    createNew: function(opts={}){
        return function(req, res, next){
                var x = [
        
                    {
                        param: 'username',
                        label: 'Username',
                        required: true,
                        type: 'string',
                        length: {min: Validation.length.username.min, max: Validation.length.username.max},
                        regex: Validation.regex.username
                    },{
                        param: 'password',
                        label: 'Password',
                        required: true,
                        type: 'string',
                        length: {min: Validation.length.password.min, max: Validation.length.password.max},
                        regex: Validation.regex.password
                    }
                ];
                    
                //create instance and pass the array x to be validated 
                let ucheck = new uCheck.validate(req).scenario(x);

                if(ucheck.hasErrors()){
                    return res.status(400).json({error: ucheck.getErrors()});
                } else {

                    var username = req.body.username;
                    var password = req.body.password;

                    User.findOne({'username': username}, '_id', function(err, exists){
                        if(exists) {
                            return res.status(400).json({error: Say.error.exists});
                        } else if (err){
                            return res.status(400).json({error: err});
                        } else {
                            var user = new User;
                            user.add({username:username, password: password});
                            user.save();
                            return res.json({});
                        }
                    });
                }
        }
    }



}

