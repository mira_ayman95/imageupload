var express = require('express');
var router = express.Router();
var uploadImageCtrl = require('../../controllers/uploadimage');
var Security = require('../../security');
var multer = require('multer');
var upload = multer({ dest: './uploads/' });


router.route('/upload')
    .post(upload.single('inputfile'), function (req, res, next) {
        if (req.file != undefined && req.file.mimetype.includes("image")) next();
        else
            res.status(400).json({ "error": "you canot do this, Bad request" })
    }, uploadImageCtrl.uploadImage());



module.exports = router;