var express      = require('express');
var router       = express.Router();
var userCtrl     = require('../../controllers/user');
var Security     = require('../../security');

//user Route
router.route('/user')

//get user profile
.get(Security.for(['user, admin']),userCtrl.getProfile())

//create new user
.post(userCtrl.createNew())

module.exports = router;