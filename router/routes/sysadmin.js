var express      = require('express');
var router       = express.Router();
var sysAdminCtrl = require('../../controllers/sysadmin');
var Security     = require('../../security');

//user Route
router.route('/system/logs')

//get user profile
.get(Security.for(['superadmin']),sysAdminCtrl.getLogs())


module.exports = router;