var express      = require('express');
var router       = express.Router();
var accessCtrl   = require('../../controllers/access');




// Security =========================================

router.route('/access')
.post(accessCtrl.login());

module.exports = router;