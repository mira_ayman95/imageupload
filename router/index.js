module.exports = function (app) {

	var nameSpace = '/api';
    var drawer  = require('../modules/drawer.js');

    var pages    = require('./routes/pages');
    var users    = require('./routes/users');
    var access   = require('./routes/access');
    var sysadmin = require('./routes/sysadmin');
	var uploadimage = require('./routes/uploadimage');
    //routes that has namespace 
	app.use(nameSpace, users);
    app.use(nameSpace, access);
    app.use(nameSpace, uploadimage);

    //no namespace
    app.use(pages);
	
    //drawing api
	drawer.APIdraw('Pages', '', pages.stack);
	drawer.APIdraw('Users', nameSpace, users.stack);
    drawer.APIdraw('Access', nameSpace, access.stack);

	
};