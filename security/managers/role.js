var Roles     = require('../../static_arch/roles.js');
var Logger    = require('../../modules/logger');
module.exports = {
    isRoleAllowed: function(req, allowedRoles){
        var allowed = allowedRoles.find(e=>e == req.ticket.data.role);
        return (allowed)? true:false;
    }
}