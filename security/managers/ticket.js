var jwt      = require('jsonwebtoken');
var config   = require('../../config');

//check if ticket exists
var isExists = function(req){
    if(req.headers.ticket && req.headers.ticket !== undefined){
        return true;
    } return false;
}

//sign ticket
var signTicket = function(data){
    return jwt.sign(data, config.JWTsecret);
}

//return decodedString/false
var verifiyTicket = function(req){
    return jwt.verify(req.headers.ticket, config.JWTsecret);
}

module.exports = {
    isExists: isExists,
    sign: signTicket,
    verifiy: verifiyTicket
}