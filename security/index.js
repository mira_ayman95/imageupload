var sessionManager = require('./managers/session');
var roleManager = require('./managers/role');
var ticketManager = require('./managers/ticket');
var Logger = require('../modules/logger');
var timeFactory = require('../modules/time-factory');


module.exports = {
    buildTicket: function(user, data, cb){
        sessionManager.login(user, function(t){
            t.data = data;
            cb(ticketManager.sign(t));
        })
    },
    for: function(allowedRoles){
        return function(req, res, next){
            if(ticketManager.isExists(req)){
                var decoded = ticketManager.verifiy(req);

                if(decoded){
                    //very important to attach decoded to req.ticket
                    req.ticket = decoded;
                    //Logger.trace('info', 'security', `decoded ticket: ${JSON.stringify(req.ticket)}`)
                    if(roleManager.isRoleAllowed(req, allowedRoles)){
                        sessionManager.validateURN(req, function(opts){
                            if(opts.valid){
                                if(sessionManager.hasVisits(opts.record)){
                                    next();
                                } else {return res.status(400).json({error: 'too many requests'})}
                            } else {return res.status(401).json({error: `unauthorized: invalid urn: ${opts.error}`})}
                        });
                    } else {return res.status(401).json({error: 'unauthorized invalid R'})}
                } else {return res.status(400).json({error: 'decode error'})} 
            } else {return res.status(401).json({error: 'unauthorized: token required'})}
        }
    }
}