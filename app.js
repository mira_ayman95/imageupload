
var throng = require('throng');
var WORKERS = process.env.WEB_CONCURRENCY || 1;
var PORT = process.env.PORT || 3000;
var ENV = process.env.NODE_ENV || 'development';

function Start() {

    var express = require('express');
    var app = express();
    var cors = require('cors');
    var config = require('./config');
    var pjson = require('./package.json');
    var Logger = require('./modules/logger.js');
    var path = require('path');
    var bodyParser = require('body-parser');
    var mongoose = require('mongoose')
    var EJS = require('ejs');
    var appCrons = require('./crons');
    var morgan = require('morgan');
    var appSeeds = require('./seed');
    var fs = require('fs');
    mongoose.Promise = global.Promise;
    mongoose.connect(config.dbURI, { useMongoClient: true });

    //cors
    app.use(cors());
    //public & static files
    app.set('views', path.join(__dirname, 'client/views'));
    app.use(express.static(path.join(__dirname, 'client')));
    // templating engine
    app.engine('html', EJS.renderFile);
    app.set('view engine', 'ejs');
    // body parser
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    //logs db connection errors
    Logger.DBconnection(mongoose);
    //log the time taken in each request
    app.use(Logger.routeTime());
    app.use(morgan('tiny'))

    if (!fs.existsSync('./uploads')) {
        fs.mkdirSync('./uploads');
    }

    //set port
    app.set('port', PORT);

    //passing routes
    require('./router')(app);

    //activate all cron jobs
    appCrons();
    //seed the application with pre defined data
    appSeeds();

    //creating server
    var server = require('http').createServer(app);

    server.listen(app.get('port'), function () {
        console.log(`${pjson.name} (${ENV}) running on port : ${app.get('port')}`);
    });


}

throng({
    workers: WORKERS,
    start: Start
});




