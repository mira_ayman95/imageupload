var ENV       = process.env.NODE_ENV || 'development';
var config    = require('../config');
var Table     = require('cli-table');


module.exports = {
    APIdraw: function (domain, baseUrl, routes) {
        if(ENV === 'development'){
            
            var table = new Table({ head: ["Request", "Path"] });
            console.log('\nAPI for ' + domain);
            console.log('\n********************************************');

            for (var key in routes) {
                if (routes.hasOwnProperty(key)) {
                    var val = routes[key];
                    if(val.route) {
                        val = val.route;
                        var _o = {};
                        _o[val.stack[0].method]  = [baseUrl + val.path];    
                        table.push(_o);
                    }       
                }
            }

            console.log(table.toString());
            return table;

        }
        
    }
}